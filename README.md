## 前言

感谢FRP作者，仓库地址：https://github.com/fatedier/frp

本文内容：使用Docker搭建FRP内网穿透服务,支持二级域名泛映射(*.test.domain.com)。

本文所有操作都假设已具备以下基础知识：

- Linux 命令基础 
- Docker 基础操作
- Nginx 基础操作

## 准备工作

- 具有公网IP的服务器
- 域名（非必需 但这里只示范使用域名）

> 这里准备了一个子域名作为演示，`*.test.domain.com`，把这个域名解析到服务器，使用`*`表示泛解析，这样可以支持同时映射多个域名到外网 也省得之后每个映射的域名都要跑域名厂商设置解析，具体要使用的泛型子域名在frp客户端配置.
>
> 创建子域名`frp.domain.com`也解析到服务器，用于查看frp监控程序面板（非必需）。

创建工作目录，执行命令：

```bash
cd /usr
mkdir frp && cd frp
```

## 服务端

- 这里使用dockerfile构建镜像
  创建dockerfile ，执行命令：

```bash
vim dockerfile
```

把以下内容填入dockerfile：

```bash
FROM amd64/alpine:3.10

LABEL maintainer="snowdream <hzxvvs@google.com>"

ENV FRP_VERSION 0.38.0

RUN cd /root \
    &&  wget --no-check-certificate -c https://github.com/fatedier/frp/releases/download/v${FRP_VERSION}/frp_${FRP_VERSION}_linux_amd64.tar.gz \
    &&  tar zxvf frp_${FRP_VERSION}_linux_amd64.tar.gz  \
    &&  cd frp_${FRP_VERSION}_linux_amd64/ \
    &&  cp frps /usr/bin/ \
    &&  mkdir -p /etc/frp \
    &&  cp frps.ini /etc/frp \
    &&  cd /root \
    &&  rm frp_${FRP_VERSION}_linux_amd64.tar.gz \
    &&  rm -rf frp_${FRP_VERSION}_linux_amd64/ 

ENTRYPOINT /usr/bin/frps -c /etc/frp/frps.ini
```

这个dockerfile执行了以下操作：

- 构建alpine-linux环境(和ubuntu发行版类似但是体积更小)
- 从github上下载frp的release版本
- 解压

此时就可以使用docker build命令进行编译镜像了，执行命令(注意：后面的点不是失误)：

```bash
docker build -t="hzx/myfrp" .
```

构建完成后命令查询镜像，执行命令：

```bash
docker images
```

## 创建服务端frpc.ini配置文件

执行命令：

```bash
cat <<EOF> frps.ini
[common]
#通讯端口，用于和客户端内网穿透传输数据的端口，可自定义
bind_port = 7000
#http监听端口，注意可能和服务器上其他服务用的80冲突，比如centos有些默认有Apache，默认80，可自定义
vhost_http_port = 8007
#https监听端口，默认443，可自定义
vhost_https_port = 4437
#通过浏览器查看 frp 的状态以及代理统计信息展示(端口、用户名、密码)，可自定义
dashboard_port = 7500
dashboard_user = admin123
dashboard_pwd = admin123
#通讯令牌(客户端需和此处一致)
token = frp123

# frp日志配置
log_file = /var/log/frps.log
log_level = info
log_max_days = 3
EOF
```

## 创建启动程序脚本

镜像库中已经存在刚刚所构建的frp镜像了，这里为了方便使用创建一个脚本来启动它，执行命令：

```bash
cat <<EOF> start.sh
#!/bin/bash
FRP_DIR=`pwd`
docker stop myfrp
docker rm myfrp
docker run -d \
    --restart always \
    --network host \
    --name myfrp \
    -v ${FRP_DIR}/frps.ini:/etc/frp/frps.ini \
    hzx/myfrp
EOF
```

## 配置域名

这里将服务器本地端口使用Nginx反向代理到泛域名，设置以下两个服务器本地端口反向代理到域名：

`127.0.0.1:8007 >>> *.test.domain.com`

`127.0.0.1:7500 >>> frp.domain.com`

这样我们就可以使用*.test.domain.com访问内网服务 和 使用frp.domain.com访问frp监控程序面板。

## 客户端

注意：客户端需要使用和服务端相同的版本！

下载frp发行版后解压文件，将文件名中包含frps的文件都删除(服务端的文件在客户端用不上)，保留frpc相关文件就好。

假设我们需要将本地服务端口的8080和8081映射出外网使用域名访问
需要编辑客户端的frpc.ini配置文件：

```bash
[common]
#填写服务器公网IP地址
server_addr = 47.106.xx.xx
#通讯端口，和服务端保持一致（如果是第三方云服务器需在厂商安全组策略中开放该端口）
server_port = 7000
#通讯令牌，和服务端保持一致
token = frp123

[web]
type = http
local_port = 8080
custom_domains = a.test.domain.com

[web2]
type = http
local_port = 8081
custom_domains = b.test.domain.com
```

## 客户端启动

mac启动：客户端根目录进入终端输入命令回车：`./frpc -c ./frpc.ini`

windows 启动：双击bat运行

此时浏览器访问`a.test.domain.com`即可访问内网中127.0.0.1:8080的服务

完工！

如果你遇到了问题可下方留言

如果本文对你有帮助请`Star`













